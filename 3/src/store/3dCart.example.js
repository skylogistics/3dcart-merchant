import { setTimeout } from 'timers';
import SkyboxSDK from '@skyboxcheckout/merchant-sdk';

const dCartStyles = require('../../assets/css/3dCart.css');
const _ = require('underscore');
// Local libs
const __cnStore = require('../../config/store.json');
const api = require('../xhr');

window.Sdk = new SkyboxSDK({
  IDSTORE: __cnStore.IDSTORE,
  MERCHANT: __cnStore.MERCHANT,
  MERCHANTCODE: __cnStore.MERCHANTCODE,
  STORE_URL: __cnStore.STORE_URL,
  SUCCESSFUL_PAGE: __cnStore.SUCCESSFUL_PAGE,
  CHECKOUT_PAGE: __cnStore.CHECKOUT_PAGE,
  CHECKOUT_BUTTON_CLASS: __cnStore.CHECKOUT_BUTTON_CLASS
});

import { multipleCalculate } from './core/multipleCalculate.js';
import { synchronizeCart } from './core/synchronizeCart.js';
import { searchVariant } from './core/searchVariant.js';

(function () {
  $(function () {
    $('.chk-buttons')
      .append('<div class="skybox-checkout-payment-btn" style="margin-bottom:15px;margin-top:10px;"></div>');

    Sdk.Common().initChangeCountry(); 
    Sdk.Common().initBtnSkyCheckout();

    if ($(location).attr('href').indexOf(__cnStore.CHECKOUT_PAGE) > -1) {
      var content = $('#skybox-international-checkout');
      content.append('<h1 id="mensaje" style="text-align:center;"><img src="https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loader.gif"/></h1>');
      
      var bc_cart_id = Sdk.Common().readCookie('incompleteorderid');
      
      if (typeof bc_cart_id == 'undefined' || bc_cart_id.length === 0) {
        alert("Cart emptyt!");
        return;
      }

      Sdk.getButton().then(function (button) {
        $('.breadcrumbs').hide();
        $('.page_headers').addClass('page_headers-3dCart');
        var height = 2500;
        if (/Mobi/.test(navigator.userAgent)) {
          height = window.parent.innerHeight;
          height = height * 7;
        }

        Sdk.getCart().then(function (cart) {
          var datos = cart.DataUrl;

          var iframeUrl = button.Url + '&idCart=' + bc_cart_id + '&idStore=' + __cnStore.STORE_HASH
            + '&UrlR=' + __cnStore.STORE_URL + __cnStore.SUCCESSFUL_PAGE + '/?i={idPurchase}&'
            + datos;

          var myIframe = jQuery('#skybox-international-checkout > iframe');
          myIframe.attr('src', iframeUrl);
          myIframe.attr('height', height);
          myIframe.hide();

          myIframe.load(function () {
            $('#mensaje').html('');
            $(this).show();
          });
        });
      });
    }

    if ($(location).attr('href').indexOf(__cnStore.SUCCESSFUL_PAGE) > -1) {
      if ($('#skybox-international-checkout-invoice').length > 0) {
        Sdk.getCartInvoice().then(function (content) {
          $('#skybox-international-checkout-invoice').html(JSON.parse(content).Data.Invoice);
        });
      }
    };
  })
})();