# [0.1.0](https://bitbucket.org/skylogistics/3dcart-merchant) (2018-04-12)

### Features

* **references:** Rename main files for generate ([fed7ebb](https://bitbucket.org/skylogistics/bigcommerce-merchant/commits/fed7ebb3f4e3fa649b2cc98f65723abe32b9ee63))
* **references:** fix bug common init ([fffa3d5](https://bitbucket.org/skylogistics/bigcommerce-merchant/commits/fffa3d5a6e081f51ad7577c68560c891ad2aa824))